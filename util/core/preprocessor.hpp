#ifndef UTIL_CORE_PREPROCESSOR_HPP_
#define UTIL_CORE_PREPROCESSOR_HPP_

#define TO_STRING(x) TO_STRING_IMPL(x)
#define TO_STRING_IMPL(x) #x

#define __SRC__ __FILE__ ":" TO_STRING(__LINE__)

#define CONCATENATE_IMPL(x, y) x ## y
#define CONCATENATE(x, y) CONCATENATE_IMPL(x, y)

#define UNIQUE_NAME(prefix) CONCATENATE(prefix, __LINE__)

#ifdef __GNUC__
#define STRICT __strict__
#else
#define STRICT
#define __attribute__(x)
#endif

#ifdef __GNUC__

#define ALWAYS_INLINE inline __attribute__((__always_inline__))
#define NEVER_INLINE  __attribute__((__noinline__))
#define WARN_UNUSED __attribute__((warn_unused_result))

#elif defined(_MSC_VER)

#define ALWAYS_INLINE __forceinline
#define NEVER_INLINE  __declspec(noinline)

#if _MSC_VER >= 1700
#define WARN_UNUSED _Check_return_
#else
#define WARN_UNUSED
#endif

#else

#define ALWAYS_INLINE inline
#define NEVER_INLINE
#define WARN_UNUSED

#endif

#endif // UTIL_CORE_PREPROCESSOR_HPP_
