#ifndef UTIL_CORE_OBJECT_HPP_
#define UTIL_CORE_OBJECT_HPP_

#include <utility>

namespace util {

template <class T, class ...Args>
inline void construct(T& obj, Args... args) {
    new (obj) T(std::forward<Args>(args)...);
}

template <class T>
inline void destroy(T& obj) noexcept {
    obj.~T();
}

template <class T, class ...Args>
inline void reconstruct(T& obj, Args... args) {
    destroy(obj);
    construct(obj, std::forward<Args>(args)...);
}

} // namespace util

#endif // UTIL_CORE_OBJECT_HPP_
