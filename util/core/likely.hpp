#ifndef UTIL_CORE_LIKELY_HPP_
#define UTIL_CORE_LIKELY_HPP_

#if defined(__GNUC__) && __GNUC__ >= 4
#define LIKELY(x)   (__builtin_expect((x), 1))
#define UNLIKELY(x) (__builtin_expect((x), 0))
#else
#define LIKELY(x)   (x)
#define UNLIKELY(x) (x)
#endif

#endif // UTIL_CORE_LIKELY_HPP_
