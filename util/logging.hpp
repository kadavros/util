#ifndef UTIL_LOGGING_HPP_
#define UTIL_LOGGING_HPP_

#include <iostream>
#include <ostream>
#include <sstream>
#include <cstdint>
#include <chrono>
#include <ctime>
#include <cstddef>
#include <iomanip>
#include <mutex>

#include <util/thread_id.hpp>

//  Thread-safe localtime_r()
#include <time.h>

namespace util {

inline std::ostream& print_current_localtime(std::ostream& stream)
{
    using namespace std;
    using namespace chrono;

    ios_base::fmtflags prev_flags(stream.flags());
    stream.flags(std::ios::dec | std::ios::fixed);
    char prev_fill_char = stream.fill();
    stream.fill('0');

    high_resolution_clock::time_point p = high_resolution_clock::now();
    milliseconds time_ms = duration_cast<milliseconds>(p.time_since_epoch());
    seconds time_s = duration_cast<seconds>(time_ms);
    time_t s = time_s.count();
    size_t ms = time_ms.count() % 1000;

    const char* date_separator = "-";
    const char* datetime_separator = " ";
    const char* time_separator = ":";

    tm t;
    localtime_r(&s, &t);
#   pragma GCC diagnostic push
#   pragma GCC diagnostic ignored "-Wmaybe-uninitialized"
    stream << setw(4) << t.tm_year + 1900 << date_separator
           << setw(2) << t.tm_mon + 1 << date_separator
           << setw(2) << t.tm_mday << datetime_separator
           << setw(2) << t.tm_hour << time_separator
           << setw(2) << t.tm_min << time_separator
           << setw(2) << t.tm_sec << '.'
           << setw(3) << ms;
#   pragma GCC diagnostic pop

    stream.fill(prev_fill_char);
    stream.flags(prev_flags);

    return stream;
}

enum class log_level: unsigned char
{
    all, trace, debug, info, warn, error, fatal, off
};

const char* to_string(log_level level)
{
    const char* name;
    switch (level) {
    case log_level::trace: name = "TRACE"; break;
    case log_level::debug: name = "DEBUG"; break;
    case log_level::info:  name = "INFO";  break;
    case log_level::warn:  name = "WARN";  break;
    case log_level::error: name = "ERROR"; break;
    case log_level::fatal: name = "FATAL"; break;
    default: name = "UNKNOWN";
    }
    return name;
}

template <class Logger>
class log_message
{
public:

    using logger_t = Logger;

    log_message(logger_t* logger, log_level msg_level, bool enabled, const char* file = "", uint32_t line = 0)
        : logger_(logger), file_(file), line_(line), enabled_(enabled), active_(true), msg_level_(msg_level)
    {
        if (is_enabled()) logger_->print_begin(this);
    }

    log_message(const log_message& other) noexcept
        : logger_(other.logger_)
        , file_(other.file_)
        , line_(other.line_)
        , enabled_(other.enabled_)
        , active_(false)
        , msg_level_(other.msg_level_)
    {}

    log_message& operator = (const log_message&) = delete;

    log_message(int) noexcept: logger_(nullptr) {}

    ~log_message() noexcept
    {
        if (is_enabled() && active_) logger_->print_end(this);
    }

    log_level msg_level() const noexcept
    { return msg_level_; }

    bool is_enabled() const noexcept
    { return logger_ && enabled_; }

    std::ostream& stream() const noexcept { return logger_t::stream(); }

    template <class T>
    log_message& operator << (const T& t)
    {
        if (is_enabled()) stream() << t;
        return *this;
    }

    log_message& operator << (std::ostream& (*pf)(std::ostream&))
    {
        if (is_enabled()) stream() << pf;
        return *this;
    }

    explicit operator bool() const noexcept
    { return stream().fail(); }

    const char*   file() const noexcept { return file_; }
    std::uint32_t line() const noexcept { return line_; }

private:

    //  TODO: pack
    logger_t*     logger_;
    const char*   file_;
    std::uint32_t line_ : 30;
    std::uint32_t enabled_ : 1;
    std::uint32_t active_ : 1;
    log_level     msg_level_;
};

class logger
{
public:

    using message_t = log_message<logger>;
    friend message_t;

    logger() noexcept
        : level_(log_level::info)
        , print_time_(1)
        , print_thread_id_(1)
        , print_log_level_(1)
        , print_src_(1)
    {}

    bool is_enabled(log_level msg_level)  const noexcept { lock_t lock(mutex_); return msg_level >= level_; }
    bool is_disabled(log_level msg_level) const noexcept { lock_t lock(mutex_); return msg_level  < level_; }
    log_level level()          const noexcept { lock_t lock(mutex_); return level_; }
    logger& set_level(log_level level)  noexcept { lock_t lock(mutex_); level_ = level; return *this; }
    bool    print_time()          const noexcept { lock_t lock(mutex_); return print_time_; }
    logger& set_print_time(bool x)      noexcept { lock_t lock(mutex_); print_time_ = x; return *this; }
    bool    print_thread_id()     const noexcept { lock_t lock(mutex_); return print_thread_id_; }
    logger& set_print_thread_id(bool x) noexcept { lock_t lock(mutex_); print_thread_id_ = x; return *this; }
    bool    print_log_level()     const noexcept { lock_t lock(mutex_); return print_log_level_; }
    logger& set_print_log_level(bool x) noexcept { lock_t lock(mutex_); print_log_level_ = x; return *this; }
    bool    print_src()           const noexcept { lock_t lock(mutex_); return print_src_; }
    logger& set_print_src(bool x)       noexcept { lock_t lock(mutex_); print_src_ = x; return *this; }

    message_t log(log_level level = log_level::info, const char* file = "", uint32_t line = 0) noexcept
    { return message_t(this, level, is_enabled(level), file, line); }

private:

    static std::stringstream& stream()
    {
        static thread_local std::stringstream stream_;
        return stream_;
    }

    void clear_stream()
    {
        stream().str(std::string());
        stream().clear();
    }

    void print_begin(message_t* p)
    {
        clear_stream();
        if (print_time()) { print_current_localtime(stream()) << " "; }
        if (print_thread_id()) { stream() << "[" << thread_id() << "] "; }
        if (print_log_level()) { stream() << to_string(p->msg_level()) << " "; }
    }

    void print_end(message_t* p) noexcept
    {
        try {
            if (print_src() && p->line() != 0) {
                stream() << " (" << p->file() << ":" << p->line() << ")";
            }
            stream() << '\n';
            if (!stream().fail()) {
                std::cout << stream().rdbuf();
            }
        } catch (...) {}
    }

    using mutex_t = std::mutex;
    using lock_t = std::lock_guard<mutex_t>;
    mutable mutex_t mutex_;
    log_level level_;
    std::uint8_t print_time_ : 1;
    std::uint8_t print_thread_id_ : 1;
    std::uint8_t print_log_level_ : 1;
    std::uint8_t print_src_ : 1;
};

} // namespace util

inline util::logger& util_logger_instance() noexcept
{
    static util::logger log;
    return log;
}

#define LOG_MSG(level) \
    util_logger_instance().is_disabled(level) ? \
        util::logger::message_t(0) : util_logger_instance().log(level, __FILE__, __LINE__)

#define LOG_TRACE_IMPL LOG_MSG(util::log_level::trace)
#define LOG_DEBUG_IMPL LOG_MSG(util::log_level::debug)
#define LOG_INFO_IMPL  LOG_MSG(util::log_level::info)
#define LOG_WARN_IMPL  LOG_MSG(util::log_level::warn)
#define LOG_ERROR_IMPL LOG_MSG(util::log_level::error)
#define LOG_FATAL_IMPL LOG_MSG(util::log_level::fatal)

#define LOG(LEVEL) LOG_ ## LEVEL ## _IMPL

#define LOGGER() util_logger_instance()

#endif // UTIL_LOGGING_HPP_
