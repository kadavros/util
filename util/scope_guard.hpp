#ifndef UTIL_SCOPE_EXIT_HPP_
#define UTIL_SCOPE_EXIT_HPP_

/*---------------------------------------------------------------------------
    SCOPE_EXIT - RAII facilities

    Example:

#include <util/scope_guard.hpp>
#include <iostream>

//  Example with SCOPE_EXIT macro.
void test1()
{
    using namespace std;

    int x = 123;
    double y = 3.1415;

    SCOPE_EXIT(cout << "End" "\n");
    SCOPE_EXIT(cout << "The" "\n");
    SCOPE_EXIT(
        cout << "Hello world!" "\n"
             << "x: " << x << "\n"
             << "y: " << y << "\n"
    );

    //  Prints:
    //  Hello world!
    //  x: 123
    //  y: 3.1415
    //  The
    //  End
}

//  Same example without macro.
void test2()
{
    using namespace std;

    int x = 123;
    double y = 3.1415;

    auto scope1 = util::make_scope_guard([](){ cout << "End" "\n"; });
    auto scope2 = util::make_scope_guard([](){ cout << "The" "\n"; });
    auto scope3 = util::make_scope_guard([&](){
        cout << "Hello world!\n"
             << "x: " << x << "\n"
             << "y: " << y << "\n";
    });

    //  Prints:
    //  Hello world!
    //  x: 123
    //  y: 3.1415
    //  The
    //  End
}

int main()
{
    test1();
    test2();
}

-----------------------------------------------------------------------------*/

#include <util/core/preprocessor.hpp>
#include <utility>
#include <type_traits>

namespace util {

template <class Lambda>
class scope_guard
{
    Lambda action_;
    bool activated_ = true;
    void exec_if() { if (activated_) { action_(); } }
public:
    scope_guard(Lambda&& action): action_(std::forward<Lambda>(action)) {}
    void release() noexcept { activated_ = false; }
    void exec() { exec_if(); release(); }
    ~scope_guard() { exec_if(); }
};

template <typename T>
struct scope_guard_lambda
{
    typedef typename std::remove_reference<T>::type U;
    typedef typename std::conditional<
        !std::is_lvalue_reference<T>::value,
        T, typename std::conditional<(sizeof(U) <= sizeof(U*)), U, U&>::type
    >::type type;
};

template <class T> using scope_guard_lambda_t = typename scope_guard_lambda<T>::type;
template <class T> using scope_guard_return_t = scope_guard<scope_guard_lambda_t<T>>;

template <class T>
inline scope_guard_return_t<T> make_scope_guard(T&& lambda)
{
    return scope_guard_return_t<T>(std::forward<scope_guard_lambda_t<T>>(lambda));
}

} // namespace util

#define SCOPE_EXIT(...) \
    auto UNIQUE_NAME(scope_guard_lambda_) = [&](){ __VA_ARGS__; }; \
    auto UNIQUE_NAME(scope_guard_instance_) = \
        util::make_scope_guard(UNIQUE_NAME(scope_guard_lambda_));

#endif // UTIL_SCOPE_EXIT_HPP_
