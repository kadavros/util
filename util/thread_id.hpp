#ifndef UTIL_THREAD_ID_HPP_
#define UTIL_THREAD_ID_HPP_

#include <cstdint>
#include <util/windows_header.hpp>
#if defined(__linux__)
#   include <unistd.h>
#   include <sys/syscall.h>
#   include <sys/types.h>
#endif

namespace util {

inline std::uint64_t thread_id()
{
#   if defined(_WIN32)
    return (std::uint64_t) GetCurrentThreadId();
#   elif defined(__linux__)
    return (std::uint64_t) syscall(SYS_gettid);
#   else
    return 0;
#   endif
}

} // namespace util

#endif // UTIL_THREAD_ID_HPP_
