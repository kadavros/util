#ifndef UTIL_ARRAY_SLICE_HPP_
#define UTIL_ARRAY_SLICE_HPP_

#include <stddef.h>
#include <iterator>
#include <stdexcept>
#include <algorithm>
#include <utility>

namespace util {

template <class T>
class array_slice
{
public:
    typedef T value_type;
    typedef value_type& reference;
    typedef const reference const_reference;
    typedef value_type* pointer;
    typedef const pointer const_pointer;
    typedef pointer iterator;
    typedef const_pointer const_iterator;
    typedef std::reverse_iterator<iterator> reverse_iterator;
    typedef std::reverse_iterator<const_iterator> const_reverse_iterator;
    typedef ptrdiff_t difference_type;
    typedef size_t size_type;

    array_slice() noexcept: data_(nullptr), size_(0) {}
    array_slice(pointer data, size_type size) noexcept: data_(data), size_(size) {}
    array_slice(array_slice&& other) noexcept: data_(other.data_), size_(other.size_) { other.clear(); }
    array_slice(const array_slice&) = delete;
    array_slice& operator =(const array_slice&) = delete;

    iterator begin() noexcept { return data(); }
    const_iterator begin() const noexcept { return data(); }
    iterator end() noexcept { return data() + size(); }
    const_iterator end() const noexcept { return data() + size(); }
    reverse_iterator rbegin() noexcept { return reverse_iterator(end()); }
    const_reverse_iterator rbegin() const noexcept { return const_reverse_iterator(end()); }
    reverse_iterator rend() noexcept { return reverse_iterator(begin()); }
    const_reverse_iterator rend() const noexcept { return const_reverse_iterator(begin()); }
    const_iterator cbegin() const noexcept { return data(); }
    const_iterator cend() const noexcept { return data() + size(); }
    const_reverse_iterator crbegin() const noexcept { return const_reverse_iterator(end()); }
    const_reverse_iterator crend() const noexcept { return const_reverse_iterator(begin()); }

    size_type size() const noexcept { return size_; }
    size_type max_size() const noexcept { return size_type(-1)/sizeof(value_type); }
    bool empty() const noexcept { return size() == 0; }

    reference operator [] (size_type n) noexcept { return data()[n]; }
    const_reference operator [] (size_type n) const noexcept { return data()[n]; }
    reference at(size_type n) { if (n >= size()) { throw std::out_of_range(""); } return data()[n]; }
    const_reference at(size_type n) const { if (n >= size()) { throw std::out_of_range(""); } return data()[n]; }
    reference front() noexcept { return data()[0]; }
    const_reference front() const noexcept { return data()[0]; }
    reference back() noexcept { return data()[size() - 1]; }
    const_reference back() const noexcept { return data()[size() - 1]; }
    pointer data() const noexcept { return data_; }

    void assign(pointer data, size_type size) noexcept { data_ = data; size_ = size; }
    void assign(pointer begin, pointer end) noexcept { data_ = begin; size_ = end - begin; }
    void swap(array_slice& x) { std::swap(*this, x); }
    void clear() noexcept { data_ = nullptr; size_ = 0; }

    void pop_front() noexcept { ++data_; --size_; }
    void pop_back() noexcept { --size_; }

private:
    pointer data_;
    size_type size_;
};

} // namespace util

#endif // UTIL_ARRAY_SLICE_HPP_
