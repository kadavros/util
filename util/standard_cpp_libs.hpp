#ifndef UTIL_STANDARD_CPP_LIBS_HPP_
#define UTIL_STANDARD_CPP_LIBS_HPP_

#define UTIL_HAS_HEADER_CUCHAR 0
#define UTIL_HAS_HEADER_CODECVT 0

//-----------------------------------------------------------------------------
//  C headers
//-----------------------------------------------------------------------------

#include <cassert>
#include <cctype>
#include <cerrno>
#include <cfloat>
#include <ciso646>
#include <climits>
#include <clocale>
#include <cmath>
#include <csetjmp>
#include <csignal>
#include <cstdarg>
#include <cstddef>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <cwchar>
#include <cwctype>

//  new in C++11
#include <cfenv>
#include <cinttypes>
#include <cstdbool>
#include <cstdint>
#include <ctgmath>

#if UTIL_HAS_HEADER_CUCHAR
#include <cuchar>
#else
#include <uchar.h>
namespace std {
    using ::c16rtomb;
    using ::c32rtomb;
    using ::mbrtoc16;
    using ::mbrtoc32;
}
#endif

//-----------------------------------------------------------------------------
//  C++ headers
//-----------------------------------------------------------------------------

//  Containers

#include <deque>
#include <list>
#include <map>
#include <queue>
#include <set>
#include <stack>
#include <vector>

//  new in C++11
#include <array>
#include <forward_list>
#include <unordered_map>
#include <unordered_set>

//  Input/Output

#include <fstream>
#include <iomanip>
#include <ios>
#include <iosfwd>
#include <iostream>
#include <istream>
#include <ostream>
#include <sstream>
#include <streambuf>

//  Multi-threading

//  new in C++11
#include <atomic>
#include <condition_variable>
#include <future>
#include <mutex>
#include <thread>

//  Other

#include <algorithm>
#include <bitset>
#include <complex>
#include <exception>
#include <functional>
#include <iterator>
#include <limits>
#include <locale>
#include <memory>
#include <new>
#include <numeric>
#include <stdexcept>
#include <string>
#include <typeinfo>
#include <utility>
#include <valarray>

//  new in C++11
#include <chrono>
#if UTIL_HAS_HEADER_CODECVT
#include <codecvt>
#endif
#include <initializer_list>
#include <random>
#include <ratio>
#include <regex>
#include <system_error>
#include <tuple>
#include <typeindex>
#include <type_traits>

#endif // UTIL_STANDARD_CPP_LIBS_HPP_
