#ifndef UTIL_WINDOWS_HEADER_HPP_
#define UTIL_WINDOWS_HEADER_HPP_

#define UTIL_USE_WIN32_LEAN_AND_MEAN 1

#if defined(_WIN32)
#   if !defined(WIN32_LEAN_AND_MEAN) && UTIL_USE_WIN32_LEAN_AND_MEAN
#       define WIN32_LEAN_AND_MEAN
#       include <windows.h>
#       undef WIN32_LEAN_AND_MEAN
#   else
#       include <windows.h>
#   endif
#endif

#endif // UTIL_WINDOWS_HEADER_HPP_
