#ifndef UTIL_CORE_HPP_
#define UTIL_CORE_HPP_

#include <util/core/likely.hpp>
#include <util/core/object.hpp>
#include <util/core/preprocessor.hpp>

#endif // UTIL_CORE_HPP_
