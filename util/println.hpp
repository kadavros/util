#ifndef UTIL_PRINTLN_HPP_
#define UTIL_PRINTLN_HPP_

#include <ostream>
#include <iostream>
#include <iomanip>

namespace util {

class println
{
public:
    println(const char* s = "\n"): s_(s) {}
    ~println() { std::cout << s_; }

    template <class T>
    println& operator << (const T& t)
    { std::cout << t; return *this; }

    println& operator << (std::ostream& (*pf)(std::ostream&))
    { std::cout << pf; return *this; }

private:
    const char* s_;
};

} // namespace util

#endif // UTIL_PRINTLN_HPP_
