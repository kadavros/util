#ifndef UTIL_SLEEP_HPP_
#define UTIL_SLEEP_HPP_

#include <chrono>
#include <thread>

namespace util {

inline void sleep_seconds(double sec) noexcept
{
    std::this_thread::sleep_for(std::chrono::duration<double>(sec));
}

} // namespace util

#endif // UTIL_SLEEP_HPP_
