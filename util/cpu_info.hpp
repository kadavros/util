#ifndef UTIL_CPU_INFO_HPP_
#define UTIL_CPU_INFO_HPP_

/*---------------------------------------------------------------------------
    CPU feature detection

    Example:

#include <util/cpu_info.hpp>
#include <iostream>

int main()
{
    using namespace std;
    using namespace util;

    cout << "CPU features test:\n"
         << "MMX:     " << cpu_has_feature(CPU_X86_FEATURE_MMX) << "\n"
         << "SSE 4.2: " << cpu_has_feature(CPU_X86_FEATURE_SSE4_2) << "\n"
         << "AVX:     " << cpu_has_feature(CPU_X86_FEATURE_AVX) << "\n";
}
-----------------------------------------------------------------------------*/

//  TODO: add MSVC support

#include <stdint.h>
#include <stddef.h>

#if defined(__GNUC__)

#include <cpuid.h>

namespace util {

//  Sources:
//  - Intel 64 and IA-32 Architectures Software Developer’s Manual
//  - http://sandpile.org/x86/cpuid.htm

enum cpu_feature: uint32_t
{
    CPU_X86_FEATURE_FPU        = (0*32 +  0), // Floating Point Unit On-Chip
    CPU_X86_FEATURE_VME        = (0*32 +  1), // Virtual 8086 Mode Enhancements
    CPU_X86_FEATURE_DE         = (0*32 +  2), // Debugging Extensions
    CPU_X86_FEATURE_PSE        = (0*32 +  3), // Page Size Extension
    CPU_X86_FEATURE_TSC        = (0*32 +  4), // Time Stamp Counter
    CPU_X86_FEATURE_MSR        = (0*32 +  5), // Model Specific Registers RDMSR and WRMSR Instructions
    CPU_X86_FEATURE_PAE        = (0*32 +  6), // Physical Address Extension
    CPU_X86_FEATURE_MCE        = (0*32 +  7), // Machine Check Exception
    CPU_X86_FEATURE_CX8        = (0*32 +  8), // CMPXCHG8B Instruction
    CPU_X86_FEATURE_APIC       = (0*32 +  9), // APIC On-Chip
    //CPU_X86_FEATURE_         = (0*32 + 10), // Reserved
    CPU_X86_FEATURE_SEP        = (0*32 + 11), // SYSENTER and SYSEXIT Instructions
    CPU_X86_FEATURE_MTRR       = (0*32 + 12), // Memory Type Range Registers
    CPU_X86_FEATURE_PGE        = (0*32 + 13), // Page Global Bit
    CPU_X86_FEATURE_MCA        = (0*32 + 14), // Machine Check Architecture
    CPU_X86_FEATURE_CMOV       = (0*32 + 15), // Conditional Move Instructions
    CPU_X86_FEATURE_PAT        = (0*32 + 16), // Page Attribute Table
    CPU_X86_FEATURE_PSE36      = (0*32 + 17), // 36-Bit Page Size Extension
    CPU_X86_FEATURE_PSN        = (0*32 + 18), // Processor Serial Number
    CPU_X86_FEATURE_CLFSH      = (0*32 + 19), // CLFLUSH Instruction
    //CPU_X86_FEATURE_         = (0*32 + 20), // Reserved
    CPU_X86_FEATURE_DS         = (0*32 + 21), // Debug Store
    CPU_X86_FEATURE_ACPI       = (0*32 + 22), // Thermal Monitor and Software Controlled Clock Facilities
    CPU_X86_FEATURE_MMX        = (0*32 + 23), // Intel MMX Technology
    CPU_X86_FEATURE_FXSR       = (0*32 + 24), // FXSAVE and FXRSTOR Instructions
    CPU_X86_FEATURE_SSE        = (0*32 + 25), // SSE
    CPU_X86_FEATURE_SSE2       = (0*32 + 26), // SSE2
    CPU_X86_FEATURE_SS         = (0*32 + 27), // Self Snoop
    CPU_X86_FEATURE_HTT        = (0*32 + 28), // Max APIC IDs reserved field is Valid
    CPU_X86_FEATURE_TM         = (0*32 + 29), // Thermal Monitor
    //CPU_X86_FEATURE_         = (0*32 + 30), // Reserved
    CPU_X86_FEATURE_PBE        = (0*32 + 31), // Pending Break Enable

    CPU_X86_FEATURE_SSE3       = (1*32 +  0), // Streaming SIMD Extensions 3
    CPU_X86_FEATURE_PCLMULQDQ  = (1*32 +  1), // PCLMULQDQ instruction
    CPU_X86_FEATURE_DTES64     = (1*32 +  2), // 64-bit DS Area
    CPU_X86_FEATURE_MONITOR    = (1*32 +  3), // MONITOR/MWAIT
    CPU_X86_FEATURE_DS_CPL     = (1*32 +  4), // CPL Qualified Debug Store
    CPU_X86_FEATURE_VMX        = (1*32 +  5), // Virtual Machine Extensions
    CPU_X86_FEATURE_SMX        = (1*32 +  6), // Safer Mode Extensions
    CPU_X86_FEATURE_EIST       = (1*32 +  7), // Enhanced Intel SpeedStep technology
    CPU_X86_FEATURE_TM2        = (1*32 +  8), // Thermal Monitor 2
    CPU_X86_FEATURE_SSSE3      = (1*32 +  9), // Supplemental Streaming SIMD Extensions 3
    CPU_X86_FEATURE_CNXT_ID    = (1*32 + 10), // L1 Context ID
    CPU_X86_FEATURE_SDBG       = (1*32 + 11), // IA32_DEBUG_INTERFACE MSR for silicon debug
    CPU_X86_FEATURE_FMA        = (1*32 + 12), // FMA extensions using YMM state
    CPU_X86_FEATURE_CMPXCHG16B = (1*32 + 13), // CMPXCHG16B instruction
    CPU_X86_FEATURE_XTPR       = (1*32 + 14), // xTPR Update Control
    CPU_X86_FEATURE_PDCM       = (1*32 + 15), // Perfmon and Debug Capability
    //CPU_X86_FEATURE_         = (1*32 + 16), // Reserved
    CPU_X86_FEATURE_PCID       = (1*32 + 17), // Process-context identifiers
    CPU_X86_FEATURE_DCA        = (1*32 + 18), // Ability to prefetch data from a memory mapped device
    CPU_X86_FEATURE_SSE4_1     = (1*32 + 19), // SSE4.1
    CPU_X86_FEATURE_SSE4_2     = (1*32 + 20), // SSE4.2
    CPU_X86_FEATURE_X2APIC     = (1*32 + 21), // x2APIC
    CPU_X86_FEATURE_MOVBE      = (1*32 + 22), // MOVBE instruction
    CPU_X86_FEATURE_POPCNT     = (1*32 + 23), // POPCNT instruction
    CPU_X86_FEATURE_TSC_DEADLINE = (1*32 + 24), // Processor’s local APIC timer supports one-shot operation using a TSC deadline value
    CPU_X86_FEATURE_AESNI      = (1*32 + 25), // AESNI instruction extensions
    CPU_X86_FEATURE_XSAVE      = (1*32 + 26), // XSAVE/XRSTOR processor extended states feature, the XSETBV/XGETBV instructions, and XCR0
    CPU_X86_FEATURE_OSXSAVE    = (1*32 + 27), // OS has set CR4.OSXSAVE[bit 18] to enable XSETBV/XGETBV instructions to access XCR0 and to support processor extended state management using XSAVE/XRSTOR.
    CPU_X86_FEATURE_AVX        = (1*32 + 28), // AVX instruction extensions
    CPU_X86_FEATURE_F16C       = (1*32 + 29), // 16-bit floating-point conversion instructions
    CPU_X86_FEATURE_RDRAND     = (1*32 + 30), // RDRAND instruction
    //CPU_X86_FEATURE_         = (1*32 + 31), // Not used

    CPU_X86_FEATURE_FSGSBASE   = (2*32 +  0), // FSGSBASE.
    CPU_X86_FEATURE_TSC_ADJUST = (2*32 +  1), // IA32_TSC_ADJUST MSR
    CPU_X86_FEATURE_SGX        = (2*32 +  2), // SGX
    CPU_X86_FEATURE_BMI1       = (2*32 +  3), // BMI1
    CPU_X86_FEATURE_HLE        = (2*32 +  4), // HLE
    CPU_X86_FEATURE_AVX2       = (2*32 +  5), // AVX2
    //CPU_X86_FEATURE_         = (2*32 +  6), // Reserved
    CPU_X86_FEATURE_SMEP       = (2*32 +  7), // Supervisor-Mode Execution Prevention
    CPU_X86_FEATURE_BMI2       = (2*32 +  8), // BMI2
    CPU_X86_FEATURE_ERMS       = (2*32 +  9), // Enhanced REP MOVSB/STOSB
    CPU_X86_FEATURE_INVPCID    = (2*32 + 10), // INVPCID instruction
    CPU_X86_FEATURE_RTM        = (2*32 + 11), // RTM
    CPU_X86_FEATURE_PQM        = (2*32 + 12), // Platform Quality of Service Monitoring (PQM) capability
    CPU_X86_FEATURE_DEPFPP     = (2*32 + 13), // FP instruction pointer and FP data pointer are deprecated
    CPU_X86_FEATURE_MPX        = (2*32 + 14), // MPX
    CPU_X86_FEATURE_PQE        = (2*32 + 15), // Platform Quality of Service Enforcement (PQE) capability
    CPU_X86_FEATURE_AVX512F    = (2*32 + 16), // AVX512F
    CPU_X86_FEATURE_AVX512DQ   = (2*32 + 17), // AVX512DQ
    CPU_X86_FEATURE_RDSEED     = (2*32 + 18), // RDSEED
    CPU_X86_FEATURE_ADX        = (2*32 + 19), // ADX
    CPU_X86_FEATURE_SMAP       = (2*32 + 20), // SMAP
    CPU_X86_FEATURE_AVX512IFMA = (2*32 + 21), // AVX512IFMA
    CPU_X86_FEATURE_PCOMMIT    = (2*32 + 22), // PCOMMIT
    CPU_X86_FEATURE_CLFLUSHOPT = (2*32 + 23), // CLFLUSHOPT
    CPU_X86_FEATURE_CLWB       = (2*32 + 24), // CLWB
    CPU_X86_FEATURE_PT         = (2*32 + 25), // Processor Trace
    CPU_X86_FEATURE_AVX512PF   = (2*32 + 26), // AVX512PF
    CPU_X86_FEATURE_AVX512ER   = (2*32 + 27), // AVX512ER
    CPU_X86_FEATURE_AVX512CD   = (2*32 + 28), // AVX512CD
    CPU_X86_FEATURE_SHA        = (2*32 + 29), // SHA
    CPU_X86_FEATURE_AVX512BW   = (2*32 + 30), // AVX512BW
    CPU_X86_FEATURE_AVX512VL   = (2*32 + 31), // AVX512VL

    CPU_X86_FEATURE_MAX_
};

struct cpuid_data
{
    unsigned int eax, ebx, ecx, edx;
};

inline bool cpuid(unsigned int level, cpuid_data& data)
{
    return __get_cpuid(level, &data.eax, &data.ebx, &data.ecx, &data.edx);
}

class cpu_features_data
{
public:
    cpu_features_data() noexcept
    {
        cpuid_data data;
        cpuid(1, data);
#       pragma GCC diagnostic push
#       pragma GCC diagnostic ignored "-Wmaybe-uninitialized"
        data_[0] = data.edx;
        data_[1] = data.ecx;
        cpuid(7, data);
        data_[2] = data.ebx;
#       pragma GCC diagnostic pop
    }

    bool has_feature(cpu_feature id) const noexcept
    {
        size_t word_id = id / 32;
        size_t bit_id = id & 31;
        return data_[word_id] & (1ul << bit_id);
    }

private:
    uint32_t data_[(CPU_X86_FEATURE_MAX_ + 31)/32];
};

inline bool cpu_has_feature(cpu_feature id)
{
    static const cpu_features_data cpu;
    return cpu.has_feature(id);
}

} // namespace util

#endif

#endif // UTIL_CPU_INFO_HPP_
