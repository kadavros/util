#ifndef UTIL_ELAPSED_TIMER_HPP_
#define UTIL_ELAPSED_TIMER_HPP_

/*---------------------------------------------------------------------------
    elapsed_timer - class for measuring elapsed time.

    Example:

#include <util/elapsed_timer.hpp>
#include <iostream>

int main()
{
    using namespace std;
    using namespace std::chrono;

    util::elapsed_timer t;

    // ... do some stuff ...
    cout << t << "\n"; // elapsed time in seconds as double

    // ... do some stuff ...
    cout << t.elapsed_seconds() << "\n";        // seconds as double (default)
    cout << t.elapsed_seconds<float>() << "\n"; // seconds as float

    // ... do some other stuff ...
    cout << t.elapsed<milliseconds>().count() << "\n"; // milliseconds as uint64_t

    // ... do some other stuff ...
    auto dt = t.elapsed();
    cout << duration_cast<milliseconds>(dt).count() << "\n";    // milliseconds as uint64_t
    cout << duration_cast<microseconds>(dt).count() << "\n";    // microseconds as uint64_t
    cout << duration_cast<nanoseconds>(dt).count() << "\n";     // nanoseconds as uint64_t
    cout << duration_cast<duration<float>>(dt).count() << "\n"; // seconds as float
}

-----------------------------------------------------------------------------*/

#include <chrono>
#include <ostream>

namespace util {

template <class Clock>
class basic_elapsed_timer
{
public:

    typedef Clock clock;

    basic_elapsed_timer() noexcept
    {
        reset();
    }

    void reset() noexcept
    {
        start_time_ = clock::now();
    }

    template <class T = double>
    T elapsed_seconds() const noexcept
    {
        return std::chrono::duration_cast<std::chrono::duration<T>>(elapsed()).count();
    }

    template <class Duration = std::chrono::nanoseconds>
    Duration elapsed() const noexcept
    {
        return std::chrono::duration_cast<Duration>(current_time() - start_time());
    }

    typename clock::time_point start_time() const noexcept
    {
        return start_time_;
    }

    static typename clock::time_point current_time() noexcept
    {
        return clock::now();
    }

private:
    typename clock::time_point start_time_;
};

using elapsed_timer = basic_elapsed_timer<std::chrono::steady_clock>;

template <class Char, class Traits, class Clock>
inline std::basic_ostream<Char, Traits>&
operator << (std::basic_ostream<Char, Traits>& stream, const basic_elapsed_timer<Clock>& t)
{
    stream << t.elapsed_seconds() << "s";
    return stream;
}

} // namespace util

#endif // UTIL_ELAPSED_TIMER_HPP_
