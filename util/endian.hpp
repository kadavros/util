#ifndef UTIL_ENDIAN_HPP_
#define UTIL_ENDIAN_HPP_

#include <cstdint>
#include <ostream>

namespace util {

enum class endian
{
    little, big
};

constexpr const char* to_string(endian val) noexcept
{
    return (val == endian::little ? "little-endian" : "big-endian");
}

inline std::ostream& operator << (std::ostream& stream, endian val)
{
    stream << to_string(val);
    return stream;
}

//  TODO: replace with portable compile time constant
constexpr endian host_endian(int x = 1) noexcept
{
    return (*(char*) &x == 1) ? endian::little : endian::big;
}

namespace detail
{

constexpr std::uint16_t endian_reverse_step8(uint16_t x) noexcept
{
    return (x << 8) | (x >> 8);
}

constexpr std::uint32_t endian_reverse_step16(uint32_t x) noexcept
{
    return (x << 16) | (x >> 16);
}

constexpr std::uint32_t endian_reverse_step8(uint32_t x) noexcept
{
    return ((endian_reverse_step16(x) << 8) & 0xff00ff00) |
           ((endian_reverse_step16(x) >> 8) & 0x00ff00ff);
}

constexpr std::uint64_t endian_reverse_step32(uint64_t x) noexcept
{
    return (x << 32) | (x >> 32);
}

constexpr std::uint64_t endian_reverse_step16(uint64_t x) noexcept
{
    return ((endian_reverse_step32(x) & 0x0000FFFF0000FFFFULL) << 16) |
           ((endian_reverse_step32(x) & 0xFFFF0000FFFF0000ULL) >> 16);
}

constexpr std::uint64_t endian_reverse_step8(uint64_t x) noexcept
{
    return ((endian_reverse_step16(x) & 0x00FF00FF00FF00FFULL) << 8) |
           ((endian_reverse_step16(x) & 0xFF00FF00FF00FF00ULL) >> 8);
}

constexpr std::int16_t endian_reverse_step8(int16_t x) noexcept
{
    return (x << 8) | (x >> 8);
}

constexpr std::int32_t endian_reverse_step16(int32_t x) noexcept
{
    return (x << 16) | (x >> 16);
}

constexpr std::int32_t endian_reverse_step8(int32_t x) noexcept
{
    return ((endian_reverse_step16(x) << 8) & 0xff00ff00) |
           ((endian_reverse_step16(x) >> 8) & 0x00ff00ff);
}

constexpr std::int64_t endian_reverse_step32(int64_t x) noexcept
{
    return (x << 32) | (x >> 32);
}

constexpr std::int64_t endian_reverse_step16(int64_t x) noexcept
{
    return ((endian_reverse_step32(x) & 0x0000FFFF0000FFFFULL) << 16) |
           ((endian_reverse_step32(x) & 0xFFFF0000FFFF0000ULL) >> 16);
}

constexpr std::int64_t endian_reverse_step8(int64_t x) noexcept
{
    return ((endian_reverse_step16(x) & 0x00FF00FF00FF00FFULL) << 8) |
           ((endian_reverse_step16(x) & 0xFF00FF00FF00FF00ULL) >> 8);
}

} // namespace detail

constexpr std::uint8_t endian_reverse(std::uint8_t x) noexcept
{
    return x;
}

constexpr std::uint16_t endian_reverse(std::uint16_t x) noexcept
{
    return detail::endian_reverse_step8(x);
}

constexpr std::uint32_t endian_reverse(std::uint32_t x) noexcept
{
    return detail::endian_reverse_step8(x);
}

constexpr std::uint64_t endian_reverse(std::uint64_t x) noexcept
{
    return detail::endian_reverse_step8(x);
}

constexpr std::int8_t endian_reverse(std::int8_t x) noexcept
{
    return x;
}

constexpr std::int16_t endian_reverse(std::int16_t x) noexcept
{
    return detail::endian_reverse_step8(x);
}

constexpr std::int32_t endian_reverse(std::int32_t x) noexcept
{
    return detail::endian_reverse_step8(x);
}

constexpr std::int64_t endian_reverse(std::int64_t x) noexcept
{
    return detail::endian_reverse_step8(x);
}

} // namespace util

#endif // UTIL_ENDIAN_HPP_
