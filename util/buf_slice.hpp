#ifndef UTIL_BUF_SLICE_HPP_
#define UTIL_BUF_SLICE_HPP_

#include <sys/uio.h>
#include <stddef.h>

//  TODO: win32
//  class buf_slice: public WSABUF
//  #define _BASE_PTR  buf
//  #define _BASE_SIZE len
class buf_slice: private iovec
{
#define _BASE_PTR  iov_base
#define _BASE_SIZE iov_len
    typedef decltype(_BASE_PTR) ptr_type;
    typedef decltype(_BASE_SIZE) size_type;
public:
    buf_slice() noexcept = default;
    buf_slice(char* _ptr, size_t _size) noexcept { reset(_ptr, _size); }
    char* data() noexcept { return (char*) _BASE_PTR; }
    const char* data() const noexcept { return (const char*) _BASE_PTR; }
    size_t size() const noexcept { return (size_t) _BASE_SIZE; }
    void reset(void* _ptr, size_t _size) noexcept { _BASE_PTR = (ptr_type) _ptr, _BASE_SIZE = (size_type) _size; }
    void set_data(void* _ptr) noexcept { _BASE_PTR = (ptr_type) _ptr; }
    void set_size(size_t _size) noexcept { _BASE_SIZE = (size_type) _size; }
    void clear() noexcept { _BASE_PTR = nullptr, _BASE_SIZE = 0; }
    void skip(size_t n) noexcept { _BASE_PTR = ((char*) _BASE_PTR) + n, _BASE_SIZE -= n; }

#undef _BASE_PTR
#undef _BASE_SIZE
};

#endif // UTIL_BUF_SLICE_HPP_
