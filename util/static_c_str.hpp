#ifndef UTIL_STATIC_C_STR_HPP_
#define UTIL_STATIC_C_STR_HPP_

#include <string>
#include <cstring>
#include <cstddef>

//  String literal with static storage duration.
class static_c_str
{
    const char* s_;
    static constexpr const char* default_str_ = "";

public:
    static_c_str(): s_(default_str_) {}
    explicit static_c_str(const char* s) noexcept: s_(s) {}
    operator const char*() const noexcept { return s_; }
    bool is_set() const noexcept { return s_ == default_str_; }
    bool empty() const noexcept { return (std::strlen(s_) == 0); }
    const char* data() const noexcept { return s_; }
};

#endif // UTIL_STATIC_C_STR_HPP_
