#ifndef UTIL_MEMORY_UNITS_HPP_
#define UTIL_MEMORY_UNITS_HPP_

#define KiB(x) (1024ull * x)
#define MiB(x) (1024ull * KiB(x))
#define GiB(x) (1024ull * MiB(x))
#define TiB(x) (1024ull * GiB(x))
#define PiB(x) (1024ull * TiB(x))
#define EiB(x) (1024ull * PiB(x))
#define ZiB(x) (1024ull * EiB(x))
#define YiB(x) (1024ull * ZiB(x))

#endif // UTIL_MEMORY_UNITS_HPP_
